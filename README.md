
# Resume

[![Build Status](https://gitlab.com/tobyfoord/resume/badges/master/build.svg)](https://gitlab.com/tobyfoord/resume)


### *A responsive resume/cv site generator for GitLab Pages. Powered by Mustache templates engine & GitLab CI.*

## License©️
Resume forked from https://gitlab.com/luisfuentes/resume/

Theme designed by [Franklin Schamhart](https://dribbble.com/shots/1887983-Resume).

Resume is licensed under the MIT Open Source license. For more information, see the LICENSE file in this repository.